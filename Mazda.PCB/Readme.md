# Schemat płytki drukowanej stm32

<details>
<summary markdown="span"> <font size="3">Wejścia/wyjścia do pcb</font></summary>

- Układ pinów STM32F7
- Układ pinów BM64 (min in, audio out)

- CAN:
    - HS CAN L
    - HS CAN H
    - LS CAN L
    - LS CAN H
- Audio (3 pins):
    - Jack - wyjście audio - do wzmacniacza 
    - Jack - wejście od laptopa
    - Jack - wejście od samochodowego dodatkowego jacka

- Laptop Power button
- 6x LCB buttons
- 2x Przekaźnik chłodzenia / Sterowanie PWM 
- SWC +
- SWC -
</details>