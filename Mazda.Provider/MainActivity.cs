﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.App;
using AndroidX.AppCompat.Widget;
using Google.Android.Material.Navigation;
using Android.Content;
using Google.Android.Material.Color;
using Google.Android.Material.NavigationRail;
using Mazda.Shared;
using Mazda.Shared.AppFragmentMgr;

namespace Mazda.Provider
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, NavigationBarView.IOnItemSelectedListener
    {
        MqttClient mqttClient;
        AppFragmentManager appFragmentManager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            DynamicColors.ApplyToActivitiesIfAvailable(Application);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            appFragmentManager = new AppFragmentManager(MenuItemIDWithFragmentLayout, SupportFragmentManager, Resource.Id.frameLayout);

            Intent handler = new Intent(this, typeof(ForegroundService));
            StartService(handler);

            NavigationRailView navigationRailView = FindViewById<NavigationRailView>(Resource.Id.navigation_rail);
            navigationRailView.SetOnItemSelectedListener(this);

            var menuTransaction = SupportFragmentManager.BeginTransaction();
            menuTransaction.Replace(Resource.Id.frameLayout, new DefaultFragmentInflater(Resource.Layout.fragment_start), "Start");
            menuTransaction.Commit();

            mqttClient = MqttClient.Instance;
            mqttClient.NotifyEvent += MqttClient_NotifyEvent;

        }


        private void MqttClient_NotifyEvent(string message)
            => RunOnUiThread(() => Android.Widget.Toast.MakeText(this, message, Android.Widget.ToastLength.Short).Show());

        private static readonly MenuConfig[] MenuItemIDWithFragmentLayout = new MenuConfig[]
        {
            new MenuConfig(Resource.Id.navmenu_start, Resource.Layout.fragment_start, "Start"),
            new MenuConfig(Resource.Id.navmenu_bt, Resource.Layout.fragment_bt, "Bt"),
            new MenuConfig(Resource.Id.navmenu_sound_selector, Resource.Layout.fragment_sound_select, "SoundSelect"),
            new MenuConfig(Resource.Id.navmenu_lcd, Resource.Layout.fragment_lcd, "Lcd"),
            new MenuConfig(Resource.Id.navmenu_can, Resource.Layout.fragment_can, "Can"),
            new MenuConfig(Resource.Id.navmenu_gpio, Resource.Layout.fragment_gpio, "Gpio"),
        };


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public bool OnNavigationItemSelected(IMenuItem item) => appFragmentManager.UpdateFragmentInFragmentManager(item);
    }
}

