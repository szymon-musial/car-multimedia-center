﻿using Android.Content;
using Android.Preferences;
using System.Net;

namespace Mazda.App
{
    public class PreferenceManagerUtils
    {
        public static void SaveIp(IPAddress ip, Context context)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            var editor = prefs.Edit();
            editor.PutLong("mqtt_ip", ip.Address);
            editor.Apply();
        }

        public static long GetSavedIp(Context context)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            return prefs.GetLong("mqtt_ip", -1);
        }
    }
}