﻿using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using System;
using Mazda.Shared;

namespace Mazda.App.Fragments
{
    public class MapsSender : Fragment
    {
        MqttClient mqttClient;
        EditText drawer;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            mqttClient = MqttClient.Instance;
        }       

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_maps_sender, container, false);
            drawer = view.FindViewById<EditText>(Resource.Id.AddrEditBox);

            view.FindViewById<Button>(Resource.Id.googlemaps).Click += GoogleMapsSender_Click;
            view.FindViewById<Button>(Resource.Id.waze).Click += WazeSender_Click;

            return view;
        }

        private void GoogleMapsSender_Click(object sender, EventArgs e)
        {
            mqttClient.Send("GoogleMapsAdres", drawer.Text);
        }

        private void WazeSender_Click(object sender, EventArgs e)
        {
            mqttClient.Send("WazeMapsAdres", drawer.Text);
        }
    }
}