﻿using Android.OS;
using Android.Text;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using Mazda.Shared;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace Mazda.App.Fragments
{
    public class Settings : Fragment
    {
        Android.App.Activity activity;

        EditText drawer;
        TextView textView;
        IPAddress IP = IPAddress.Loopback;
        Button button;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            activity = Context as Android.App.Activity;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_settings, container, false);
            drawer = view.FindViewById<EditText>(Resource.Id.IPEditBox);
            drawer.TextChanged += Drawer_TextChanged;

            textView = view.FindViewById<TextView>(Resource.Id.ip_text_view);
            button = view.FindViewById<Button>(Resource.Id.setip);
            button.Click += Button_Click;

            var mqtt_ip = PreferenceManagerUtils.GetSavedIp(Context);
            if (mqtt_ip > 0)
            {
                IP = new IPAddress(mqtt_ip);
            }

            drawer.Text = IP.ToString();

            return view;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Task.Run(async () =>
            {
                UpdateTextWithVisibility($"Checking {drawer.Text}...", ViewStates.Invisible);
                if (!IPAddress.TryParse(drawer.Text, out var ip))
                {
                    UpdateTextWithVisibility($"Ip is incorrect");
                    return;
                }

                if (!await PingOKAsync(ip))
                {
                    UpdateTextWithVisibility($"{ip} is unreachable");
                    return;
                }

                MqttClient.ConfigureMqttClient(ip.ToString());
                PreferenceManagerUtils.SaveIp(ip, Context);
                UpdateTextWithVisibility("Saved new ip");
            });
        }

        private void UpdateTextWithVisibility(string text, ViewStates viewState = ViewStates.Visible)
        {
            activity?.RunOnUiThread(() =>
            {
                textView.Text = text;
                button.Visibility = viewState;
            });
        }

        private void Drawer_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(string.IsNullOrEmpty(drawer.Text))
            {
                return;
            }

            if(IPAddress.TryParse(drawer.Text, out var ip))
            {
                UpdateTextWithVisibility($"{ip} looks good");
            }
            else
            {
                UpdateTextWithVisibility($"{drawer.Text} are incorrect", ViewStates.Invisible);
            }
        }

        private async Task<bool> PingOKAsync(IPAddress iPAddress)
        {
            Ping p = new Ping();
            PingReply rep = p.Send(iPAddress);
            await Task.Delay(100);
            if (rep.Status == IPStatus.Success)
            {
                return true;
            }
            return false;
        }
    }
}