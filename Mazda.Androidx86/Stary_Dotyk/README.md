# Modyfikacja układu klawiatury dla gt911

Zamówiony przeze mnie dotyk pasujący do ramki w samochodzie w miejscu oryginalnego radia posiada dodatkowe przyciski:
- Zasilania
- Domowy
- Powrotu
- Zwiększenia głośności
- Zmniejszenia głośności

W domyślnej konfiguracji znajduje się definicja dla tego sterownika `\kernel\drivers\hid\hid-ids.h`
```c
#define USB_VENDOR_ID_ILITEK		0x222a
#define USB_DEVICE_ID_ILITEK_MULTITOUCH	0x0001
```


Warstwami pośredniczącymi między sprzętem HID a zdarzeniami z urządzeń wejściowych przekazywane dla 'InputDispatcher' są:
- Standardowy protokół wejść systemu linux `linux/input.h`
- Android EventHub, dla którego zmieniłem konfigurację

w katalogu /system/usr/keylayout/Generic.kl znajduje się plik domyślnej konfiguracji klawiatur
Aby dodać własną należy stworzyć plik według schematu:
Vendor_XXXX_Product_XXXX.kl

Gdzie w miejsce XXXX należy dodać odpowiednie dane dla naszego urządzenia
W moim przypadku wygląda to następująco:
Vendor_222a_Product_0001.kl

```bash
x86_64:/system/usr/keylayout # ls
AVRCP.kl                     Vendor_1532_Product_0900.kl
Generic.kl                   Vendor_1689_Product_fd00.kl
HP_WMI_hotkeys.kl            Vendor_1689_Product_fd01.kl
Vendor_0079_Product_0011.kl  Vendor_1689_Product_fe00.kl
Vendor_045e_Product_028e.kl  Vendor_18d1_Product_2c40.kl
Vendor_046d_Product_b501.kl  Vendor_18d1_Product_5018.kl
Vendor_046d_Product_c216.kl  Vendor_1949_Product_0401.kl
Vendor_046d_Product_c219.kl  Vendor_1bad_Product_f016.kl
Vendor_046d_Product_c21d.kl  Vendor_1bad_Product_f023.kl
Vendor_046d_Product_c21f.kl  Vendor_1bad_Product_f027.kl
Vendor_046d_Product_c294.kl  Vendor_1bad_Product_f036.kl
Vendor_046d_Product_c299.kl  Vendor_1d79_Product_0009.kl
Vendor_046d_Product_c532.kl  Vendor_222a_Product_0001.kl
Vendor_054c_Product_0268.kl  Vendor_22b8_Product_093d.kl
Vendor_0583_Product_2060.kl  Vendor_2378_Product_1008.kl
Vendor_05ac_Product_0239.kl  Vendor_2378_Product_100a.kl
Vendor_0b05_Product_4500.kl  qwerty.kl
Vendor_1038_Product_1412.kl  tincore_kb.kl
Vendor_12bd_Product_d015.kl
```


Odczyt identyfikatorów możemy dokonać na kilka sposobów:
- Sprawdzając w systemie Windows w Menadżerze urządzeń
- Wpisując w shellu w androidzie lsusb

```bash
x86_64:/system/usr/keylayout # lsusb
Bus 002 Device 002: ID 8087:0024
Bus 001 Device 001: ID 1d6b:0002
Bus 001 Device 003: ID 1bcf:288a
Bus 001 Device 002: ID 8087:0024
Bus 002 Device 001: ID 1d6b:0002
Bus 002 Device 004: ID 222a:0001
```
- odczytując plik /proc/bus/input/devices
```bash
I: Bus=0003 Vendor=222a Product=0001 Version=0111
N: Name="OpenWare Multi-Touch-V5000"
P: Phys=usb-0000:00:1d.0-1.4/input1
S: Sysfs=/devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.4/2-1.4:1.1/0003:222A:0001.0005/input/input19
U: Uniq=8964ACBC0334
H: Handlers=kbd event5
B: PROP=0
B: EV=100013
B: KEY=1000000000007 ff980000000007ff febeffdfffefffff fffffffffffffffe
B: MSC=10
```

Za pomocą logcat możemy odczytać informację o naszym urządzeniu (w moim przypadku były to 3 urządzenia. Przedstawiam tylko klawiaturę)
```bash
x86_64:/ # logcat
#........................
04-29 11:21:44.879  1591  1627 I EventHub: New device: id=18, fd=96, path='/dev/input/event5', name='OpenWare Multi-Touch-V5000', classes=0x80000001, configuration='', keyLayout='/system/usr/keylayout/Vendor_222a_Product_0001.kl', keyCharacterMap='/system/usr/keychars/Generic.kcm', builtinKeyboard=false, wakeMechanism=EPOLLWAKEUP, usingClockIoctl=true
```

Po stworzeniu odpowiedniego pliku mogę przystąpić do konfiguracji klawiszy. W moim przypadku deklaracje sprzedawcy nie pokryły się z rzeczywistością.

Analizując plik Generic.kl możemy porównując numery klawiszy i odpowiadającym im funkcjom zorientować się jaki numer klawisza ma nasze urządzenie.

Odczytu można dokonać za pomocą narzędzia KeyMap w systemie Windows.
Wynik programu pokazał rozbieżność dla klawisza powrotu: Ctrl Alt Left

Programu evtest na linuxa można było użyć w systemie android, program działa w przestrzeni użytkownika dzięki czemu w łatwy sposób mogłem do skompilować na WSL Ubuntu (architektury są takie same) oraz uruchomić w systemie.
Istnieje rozbieżność w lokalizacjach bibliotek w systemie linux oraz android, dlatego dokonałem kompilacji linkując statycznie biblioteki

```bash
szymon@SZYMON-DESKTOP:~/evtest$ gcc -o evtest evtest.c --static
szymon@SZYMON-DESKTOP:~/evtest$ readelf -l evtest

Elf file type is EXEC (Executable file)
Entry point 0x4023f0
There are 10 program headers, starting at offset 64

Program Headers:
  Type           Offset             VirtAddr           PhysAddr
                 FileSiz            MemSiz              Flags  Align
  LOAD           0x0000000000000000 0x0000000000400000 0x0000000000400000
                 0x0000000000000530 0x0000000000000530  R      0x1000
  LOAD           0x0000000000001000 0x0000000000401000 0x0000000000401000
                 0x00000000000adc3d 0x00000000000adc3d  R E    0x1000
  LOAD           0x00000000000af000 0x00000000004af000 0x00000000004af000
                 0x000000000002c568 0x000000000002c568  R      0x1000
  LOAD           0x00000000000dbb00 0x00000000004dcb00 0x00000000004dcb00
                 0x00000000000077f0 0x0000000000009120  RW     0x1000
  NOTE           0x0000000000000270 0x0000000000400270 0x0000000000400270
                 0x0000000000000020 0x0000000000000020  R      0x8
  NOTE           0x0000000000000290 0x0000000000400290 0x0000000000400290
                 0x0000000000000044 0x0000000000000044  R      0x4
  TLS            0x00000000000dbb00 0x00000000004dcb00 0x00000000004dcb00
                 0x0000000000000020 0x0000000000000060  R      0x8
  GNU_PROPERTY   0x0000000000000270 0x0000000000400270 0x0000000000400270
                 0x0000000000000020 0x0000000000000020  R      0x8
  GNU_STACK      0x0000000000000000 0x0000000000000000 0x0000000000000000
                 0x0000000000000000 0x0000000000000000  RW     0x10
  GNU_RELRO      0x00000000000dbb00 0x00000000004dcb00 0x00000000004dcb00
                 0x0000000000005500 0x0000000000005500  R      0x1
```

Jak widać powyżej nie mam zależności do zewnętrznej biblioteki, linkowanie dynamiczne skutkuje poniższym zapisem
```bash
  INTERP         0x0000000000000318 0x0000000000000318 0x0000000000000318
                 0x000000000000001c 0x000000000000001c  R      0x1
      [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
```


Po uruchomieniu programu oraz podaniu poprawnego urządzenia uzyskujemy wystarczające informacje do konfiguracji

```bash

x86_64:/system/bin # evtest /dev/input/event5
Input driver version is 1.0.1
Input device ID: bus 0x3 vendor 0x222a product 0x1 version 0x111
Input device name: "OpenWare Multi-Touch-V5000"
#.....................
# Lista kodów klawiszy deklarowanych przez producenta
#.....................
Testing ... (interrupt to exit)
Event: time 1619688550.500822, type 20 (EV_REP), code 0 (REP_DELAY), value 0
Event: time 1619688550.500822, type 20 (EV_REP), code 1 (REP_PERIOD), value 0
Event: time 1619688550.500822, type 4 (EV_MSC), code 4 (MSC_SCAN), value 700e2
Event: time 1619688550.500822, type 1 (EV_KEY), code 56 (KEY_LEFTALT), value 1
Event: time 1619688550.500822, type 4 (EV_MSC), code 4 (MSC_SCAN), value 70050
Event: time 1619688550.500822, type 1 (EV_KEY), code 105 (KEY_LEFT), value 1
Event: time 1619688550.500822, -------------- SYN_REPORT ------------
Event: time 1619688550.660813, type 4 (EV_MSC), code 4 (MSC_SCAN), value 700e2
Event: time 1619688550.660813, type 1 (EV_KEY), code 56 (KEY_LEFTALT), value 0
Event: time 1619688550.660813, type 4 (EV_MSC), code 4 (MSC_SCAN), value 70050
Event: time 1619688550.660813, type 1 (EV_KEY), code 105 (KEY_LEFT), value 0
Event: time 1619688550.660813, -------------- SYN_REPORT ------------
```

Aktualizacja układu następuje po ponownym podłączeniu urządzenia, nie jest konieczne ponowne uruchamiania urządzenia.

Poprawność ustawień oraz ewentualnych błędów można sprawdzić poleceniem logcat

W systemie kombinacja Ctrl Alt Left posiada specjalną funkcjonalność

Klawisze:
- Ctrl Alt Left
- Ctrl Alt Right
- Ctrl Alt F1
- Ctrl Alt F7

Służą do przełączania terminali. Jest to funkcjonalność zaprogramowana na poziomie jądra, pomija ona konfigurację klawiszy. Całość skutkowała tym że po naciśnięciu powrotu następowało przełączanie na inny terminal, wyjściem z GUI

Problem wyeliminowałem poprzez komendę:
```bash
ioctl /dev/tty0 0x560B
```
Blokując terminal `tty0` na który znajduje się środowisko graficzne, wyeliminowałem ten problem. Kod `0x560C` odblokowuje go ponownie

Efekt jest trwały do momentu ponownego uruchomienia urządzenia. Aby również po uruchomieniu efekt występował możemy dodać powyższą linię do skryptu .sh który będzie uruchamiany np. poprzez plik .rc przy starcie systemu 


## Przydatne Linki

Dokumentacja plików konfiguracyjnych układu klawiatury: https://source.android.com/devices/input/key-layout-files

Program do odczytu zdarzeń z urządzenia wejściowego: https://github.com/freedesktop-unofficial-mirror/evtest

Program do odczytu zdarzeń z urządzenia wejściowego windows: https://github.com/nhmkdev/KeyCap

Blokada zmiany terminali: https://groups.google.com/g/android-x86/c/Q6fGPmRAjF8

Dokumentacja urządzeń wejściowych AOSP: https://source.android.com/devices/input


# Wirtualne przyciski deklarowane przez producenta
![Wirtualne przyciski](./AliexpressOrder.png)
