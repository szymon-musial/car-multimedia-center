# Symulacja platformy ARM na urządzeniu Android x86_64

W celu zainstalowania aplikacji skompilowanych tylko na platformy ARM (np. Waze)
powstała możliwość włączenia natywnego wsparcia. Aby tego dokonać należy:

-   Zaznaczyć opcję Settings \> Apps Compatibility \> Enable Native Bridge

-   uruchomić skrypt instalujący bibliotekę houdini.sfs

## Modyfikacja skryptu system/bin/enable_nativebridge.sh

Narzędzie wget dostępne w systemie ma problem z pobieraniem zawartości
korzystając z SSL.

``` bash
if [ -z "$1" ]; then
 if [ "`uname -m`" = "x86_64" ]; then
  v=7_y
  url=http://goo.gl/SBU3is
 else
  v=7_x
  url=http://goo.gl/0IJs40
 fi
else
 v=7_z
 url=http://goo.gl/FDrxVN
fi

Connecting to goo.gl (216.58.215.78:80)
wget: not an http or ftp url: https://goo.gl/SBU3is
```

Serwery google jak możemy zauważyć powyżej wymuszają pobranie za pomocą https co
powoduje problem. Rozwiązać go można podmieniając adres w skrypcie. Po
kliknięciu na łącze zostaniemy przekierowani do pobrania pliku przez co możemy
pobrać adres. Z czego na dzień (08.01.2020) fragment skryptu został
zmodyfikowany do poniższej wersji

``` bash
if [ -z "$1" ]; then
	if [ "`uname -m`" = "x86_64" ]; then
		v=7_y
		url=http://zebra-mirror.ddscentral.org/dl.android-x86.org/houdini/7_y/houdini.sfs
	else
		v=7_x
		url=http://goo.gl/0IJs40
	fi
else
	v=7_z
	url=http://zebra-mirror.ddscentral.org/dl.android-x86.org/houdini/7_z/houdini.sfs
fi
```
``` bash
mount: '/system/lib/arm/libhoudini.so'->'/system/lib/libhoudini.so': No such file or directory
mount: 'houdini7_z.sfs'->'/system/lib64/arm64': No such file or directory
Connecting to zebra-mirror.ddscentral.org (78.62.182.74:80)
houdini7_z.sfs        29% |*********                      | 10867k  0:00:23 ETA
```
