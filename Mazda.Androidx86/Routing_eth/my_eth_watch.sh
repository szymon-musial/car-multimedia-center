#!/system/bin/sh

CHECK_DELAY_S=0.5
SELECTED_INTERFACE='eth0'
PC_IP='10.4.0.10/16'
STM_IP='10.4.0.20'
PC_NET='10.4.0.0/16'

exec >>/storage/emulated/0/my_eth_watch.log 2>&1
echo "start"
prev_state=$(cat /sys/class/net/$SELECTED_INTERFACE/operstate)

ip rule add from $PC_NET table main
ip rule add to $PC_NET table main

function update_eth_config()
{
    echo "$(date +"%c"): changed state from previous to up"

        ifconfig $SELECTED_INTERFACE $PC_IP up
        ip route add $PC_NET dev $SELECTED_INTERFACE table main || true
        ip route add default via $STM_IP dev $SELECTED_INTERFACE table main || true
}

update_eth_config

while true
do
    ip a | grep -A4 $SELECTED_INTERFACE | grep inet | grep -q $PC_IP
                        
    if [ $? -ne 0 ]
    then                                                                    
            update_eth_config
    fi

	sleep $CHECK_DELAY_S
done
