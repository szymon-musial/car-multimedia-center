import numpy as np
from numpy.polynomial.polynomial import polyfit
import matplotlib.pyplot as plt

from openpyxl import load_workbook

wb2 = load_workbook('swc.xlsx')
print (wb2.get_sheet_names())

worksheet1 = wb2['swc'] 

x = list(range(1, 9))
y = []

for i in x:
    y.append(worksheet1['D' + str(i+1)].value)


# swap
tmp = x
x = y
y = tmp

print (x)

plt.scatter(x, y,marker='.', label='Data for regression')

hm = np.poly1d(np.polyfit(x, y, 1))

plt.plot(np.unique(x), hm(np.unique(x)), 
         label='numpy.polyfit')

print (hm)

plt.legend()
plt.show()