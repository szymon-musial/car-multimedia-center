# Android x86 w samochodzie Mazda 3 (bk)

## Cel

Stworzenie wydajnego systemu audio/nawigacji
bazującego na systemie Android. Android Auto wymaga korzystania z telefonu,
czego chcę uniknąć.

## Oczekiwanie

Niezależny system multimedialny oparty na systemie Android gwarantujący dostęp
do natywnych aplikacji (nawet arm poprzez emulacje).

Ponadto system posiada w
przestrzeni user space linuxa niezależny od os-a serwer mqtt broker gwarantujący
połączenie protokołem mqtt oraz nie wpływa na wydajność aplikacji andorid (i
odwrotnie) Protokół używany jest do komunikacji krytycznego urządzenia STM32
który kontroluje peryferia w tym dwie magistrale (auto posiada 2xCAN ). Dane
zostaną użytkownikowi zwizualizowane.

Z poziomu laptopa można wpływać na ich
działanie. W miejsce fabrycznego auto podłączony jest wpasowany ekran dotykowy.
Poprzez możliwość podłączenia się do sieci VPN, dane mogą zostać zapisywane oraz
wykorzystywane do tworzenia logów, trasy przejazdów, statystyk.

## Sprzęt

-   Laptop Acer Aspire 5755G za zainstalowanym zmodyfikowanym Android x86 w
    wersji 7.1

-   Moduł GPS U-Blox VK-162

-   Moduł BT BM64 (obsługuje HFP, AVRCP)

-   6-kanałowy elektroniczny kontroler dźwięku PT2258

-   Moduł CAN MCP2515 Transceiver - STM F7 posiada sprzętowy kontroler can

-   STM32 NUCLEO-F746ZG - nie wymaga uruchomienia laptopa, działa niezależnie, ma
    niskopoziomowe zadania

-   Mazda 3 bk USA (automat)

-   Wyświetlacz 9 cal

-   Dotyk GT911

-   Mikrofon

-   Wzmacniacz Magnat Classic 360 (okazjonalnie)

-   Chłodzenie wodne

-   Podłączenie z komputerem instalacji gazowej STAG (Opcjonalnie)

### Zmiany

Już na etapie testów zdecydowałem się na wymiana mikroprocesora z STM32F411CEU6
na kit NUCLEO-F746ZG

-   Płytka rozwojowa ten posiada natywnie kontroler 2xCAN oraz Ethernet. Cenowo
    rozwiązanie to jest podobne jak 2 moduły mcp oraz kontroler Ethernet na SPI
    moduł UART -\> Ethernet. Co więcej mam możliwość zaimplementowania natywnie
    wg. np. STM CUBE, użycie LWIP a przez to protokołu MQTT, czy FreeRTOS

-   Komunikacja za pomocą Ethernet nie UART. Rozwiązuje to pojawiającą się
    trudność z deskryptorem USB, który powinien być jako Virtual Can oraz HID
    (klawiatura). Dodatkowo mogę podzielić aplikację na mniejsze części która
    będzie nasłuchiwała na określony Topic MQTT. Użycie HID jako klawiatury
    umożliwi mi pisanie z telefonu a bez przełączania klawiatur w android x86

-   Eliminuje to problem zmiennej numeracji portu /dev/ttyACM[0, 1]. Serownik
    GPS w pliku build.prop ma przekazaną informację o nazwie portu szeregowego,
    po podłączeniu STM jego numer zmienia się z 0 na 1. Co jest dla mnie
    problemem, oraz powoduję losowość

## Połączenia

### Do laptopa (Android x86)

-   Moduł GPS - Poprzez USB jako wirtualny port szeregowy

-   Dotyk GT911- Poprzez USB

-   Wyświetlacz - poprzez HDMI

-   Wejście mikrofonu - dla działania asystenta głosowego

-   Wyjście słuchawkowe do kontrolera dźwięku

-   STM32 - Ethernet - komunikacja

-   WiFi Client - Udostępnianie Internetu z telefonu

-   STM32 - USB HID - klawiatura

-   Kamera DVR - USB

### Do STM32

-   Laptop Ethernet - natywny kontroler - komunikacja

-   Magistrala CAN auta - natywny kontroler - odczyt danych np. spalanie
    chwilowe, tryb jazdy itp.

-   RN52 - UART - sterowanie muzyką, odbieranie połączeń, przesyłanie numeru do
    laptopa w celu wyświetlenia nazwy kontaktu, za pomocą książki telefonicznej

-   PT2258 - I2C - sterowanie wyjściami dźwięku za pomocą biblioteki Arduino
    którą dostosowałem do STM32

-   Panel sterowania wyświetlaczem - GPIO - tranzystor kluczujący(?) sterujący
    podświetleniem itp.

-   Sterowanie z kierownicy - ADC - drabinka rezystorów - w zależności od źródła
    dźwięku jak bt to np. następny utwór, a jak android to akcja klawiatury
    przewijająca utwór wtedy trafi to do Spotify czy YT

-   Ewentualne sterowanie wzmacniaczem audio

-   W przyszłości: kierunkowskazy automatyczne, moduł domykania szyb, wszystkie
    szyby automatyczne

### Wyświetlacz

-   Wejście HDMI - Laptop

-   Wejście AV - kamera cofania. Sterownik posiada złącze wejściowe GPIO które
    po podaniu sygnału wysokiego przełącza na wejście AV. Po zastosowaniu
    wejścia AV podłączonego do laptopa poza o większymi kosztami pojawia się
    problem że powiadomienia nakładają się na obraz co utrudnia wykonanie
    manewru

### kontroler dźwięku PT2258

-   Wejście BT RN52

-   Wejście dźwięku z laptopa

-   Wejście jack

-   Przekierowuje sygnał do wzmacniacza audio

## Aplikacja

-   Xamarin Android na telefon ma za zadanie odbierania Intentu "share to" z map
    google przesłanie tego do Laptopa a on następnie uruchamia nawigację.
    Telefon będzie służył również jako zdalna klawiatura

-   Xamarin Android na laptopa będzie miała za zadanie serowania oraz
    wizualizacji parametrów

-   W przyszłości: Launcher np. zmieniający orientację paska itp.

-   Zmodyfikowany Android x86. Ma on na celu natywną obsługę GPS, oraz dotyku
    GT911

### W niektórych katalogach znajdują się informacje co zostało zmienione w systemie Android

# Diagram urządzeń wraz z portami

![Diagram ](./Mazda.Docs/Project_Diagram.vpd.svg)

# Diagram Mermaid

Diagramy zostały podzielone na:
- Diagram urządzeń Pc
- Diagram urządzeń STM


```mermaid
flowchart TB

    subgraph PC_part[Diagram urządzeń Pc]
        subgraph PC_box[PC]
            PC[Laptop]
        end
    end

    subgraph STM_part[Diagram urządzeń STM]
        subgraph stm_box[STM32]
            stm[STM32F7]
        end
        microphone[Mikrofon]
        -- MiniJack --> PC

        stm <-- Ethernet --> PC
        stm <-- USB: HID --> PC
    end

```


## Diagram urządzeń Pc
```mermaid

flowchart TB

    subgraph PC[PC]
        PC_sub[Laptop]
    end

    PC_LC[Chłodzenie Pc]
    ---
    PC

    subgraph lcd_box[Wyświetlacz]
        direction TB
        LCD_Driver[Sterownik ekranu]
        -->
        LCD[Ekran: 9']
        
        Touch[Dotyk: GT911]
        -- I2C -->
        Touch_driver[Sterownik ekranu dotykowego: STM32F4]
    end

    Rear_cam[Kamera cofania]
    -- AV --> LCD_Driver

    Front_cam[Kamera DVR]
    <-- USB --> PC

    LCD_Driver <-- HDMI ----> PC
    Touch_driver <-- USB HID --> PC

    subgraph stm_box[STM32]
        direction RL
        stm[STM32F7]
    end

    stm_box <-- Ethernet --> PC
    stm_box <-- USB: HID --> PC
    stm_box <-- MiniJack --> PC

    microphone[Mikrofon]
    -- MiniJack --> PC

    gps[GPS: U-Blox VK-162]
    <-- USB: Serial --> PC
```

## Diagram urządzeń STM
```mermaid

flowchart TB

    subgraph stm_box[STM32]
        stm[STM32F7]
        ---
        stm_root[Konektory STM32, płytka: Root]
    end

    stm_box --> pwr_btm[Przycisk pc]

    subgraph PC[PC]
        PC_sub[Laptop]
    end

    adc_can[Płytka: ADC_CAN] -------> stm_box

    swc[Sterowanie z kierownicy] --> adc_can
    ecu[Samochodowy komputer pokładowy] -- CAN --> adc_can

    bt[Bluetooth BM64, płytka: Bt] <-- UART ---> stm_box

    gpio[Przekaźniki, płytka: GPIO]
    stm_box --> gpio

    gpio --> lc[Chłodzenie pc]
    gpio --> power[Zasilanie]    

    lcd[Steronik sterownika, płytka: LCD]
    stm_box --> lcd
    lcd --> LCD_Driver[Sterownik ekranu]

    volcntr[Selektor dźwięku PT2258, płytka: VolCntr]

    volcntr <-- I2C ----> stm_box

    volcntr -- MiniJack --> amp[Końcówka mocy audio]

    microphone[Mikrofon]
    -- MiniJack --> bt

    microphone -- MiniJack --> PC


    bt -- MiniJack--> volcntr
    PC -- MiniJack--> volcntr
    Aux[AUX] -- MiniJack --> volcntr

    stm_box <-- Ethernet ------> PC
    stm_box <-- USB: HID ------> PC

```