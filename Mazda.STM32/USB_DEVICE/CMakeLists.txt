cmake_minimum_required(VERSION 3.22)

#
# List of source files to compile
#
set(USB_DEVICESrc
    # Put here your source files, one in each line, relative to CMakeLists.txt file location
    ${CMAKE_CURRENT_LIST_DIR}/App/usb_device.c 
    ${CMAKE_CURRENT_LIST_DIR}/App/usbd_desc.c 
    ${CMAKE_CURRENT_LIST_DIR}/Target/usbd_conf.c 
)

#
# Include directories
#
set(USB_DEVICEInc
    # Put here your include dirs, one in each line, relative to CMakeLists.txt file location
    ${CMAKE_CURRENT_LIST_DIR}/App 
    ${CMAKE_CURRENT_LIST_DIR}/Target     
)