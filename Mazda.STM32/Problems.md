# Limity ilości publikacji mqtt 

Biblioteka kolejkuje publikacje a limity zdefinowane są w pliku:
Middlewares\Third_Party\LwIP\src\include\lwip\apps\mqtt_opts.h
```c
#define MQTT_REQ_MAX_IN_FLIGHT
````
CubeMx generuje to w pliku: lwipopts.h

Wartość powyższego parametru nie powinna przekraczać maksymelnej wartośc uint8_t

Definicja funkcji mqtt_init_requests() w Middlewares\Third_Party\LwIP\src\apps\mqtt\mqtt.c zawiera iterator właśnie typu uint8_t

Zwiększając należy również pamiętać o pamięci: https://community.st.com/s/question/0D53W00000as7G6/stm32f767-mqtt-using-lwip