#include "AppCore.h"
#include "FreeRTOS.h"
#include "main.h"

#include "lwip.h"
#include "mqtt.h"

#include <stdbool.h>
#include <stdio.h>

mqtt_client_t *app_mqtt_client;

extern bool MX_LWIP_Init_End;
extern struct netif gnetif;

bool GotIpBool = false;

static ip_addr_t mqtt_ip = IPADDR4_INIT_BYTES(10, 4, 0, 10);

static const struct mqtt_connect_client_info_t mqtt_client_info =
    {
        "STM32",
        NULL, /* user */
        NULL, /* pass */
        0,    /* keep alive */
        NULL, /* will_topic */
        NULL, /* will_msg */
        1,    /* will_qos */
        0     /* will_retain */
#if LWIP_ALTCP && LWIP_ALTCP_TLS
        ,
        NULL
#endif
};

void GotIp()
{
  SEGGER_SYSVIEW_PrintfHost("\nKlient: ");
  PrintAddress(&gnetif.ip_addr);

  SEGGER_SYSVIEW_PrintfHost("Brama: ");
  PrintAddress(&gnetif.gw);

  app_mqtt_client = mqtt_client_new();

  err_t er = mqtt_client_connect(app_mqtt_client,
                                 &mqtt_ip, MQTT_PORT,
                                 mqtt_connection_cb, LWIP_CONST_CAST(void *, &mqtt_client_info),
                                 &mqtt_client_info);
  SEGGER_SYSVIEW_PrintfHost("GotIp Mqtt connect result %d\n", er);
}

void PrintAddress(ip4_addr_t *ipv4)
{
  SEGGER_SYSVIEW_PrintfHost("%d:%d:%d:%d\n", ip4_addr1(ipv4), ip4_addr2(ipv4), ip4_addr3(ipv4), ip4_addr4(ipv4));
}

void ConnectTaskFun(void *argument)
{
    MX_LWIP_Init();

  //osDelay(5000);
  while (1)
  {
    if (gnetif.ip_addr.addr != 0)
    {
      if (!GotIpBool && MX_LWIP_Init_End)
      {
        GotIp();
        GotIpBool = true;
      }
      osDelay(1000);
    }
    else
    {
      GotIpBool = false;
      osDelay(100);
    }
  }
  /* Infinite loop */
  for (;;)
  {
    osDelay(1);
  }
}

void mqtt_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status)
{
  if (status == MQTT_CONNECT_ACCEPTED)
  {
    SEGGER_SYSVIEW_PrintfHost("mqtt_connection_cb: Successfully connected\n");

    /* Setup callback for incoming publish requests */
    mqtt_set_inpub_callback(client, mqtt_incoming_publish_cb, mqtt_incoming_data_cb, arg);

    SubscribeAll(client, arg);
  }
  else
  {
    SEGGER_SYSVIEW_PrintfHost("mqtt_connection_cb: Disconnected, reason: %d\n", status);

    err_t er = mqtt_client_connect(app_mqtt_client,
                                   &mqtt_ip, MQTT_PORT,
                                   mqtt_connection_cb, LWIP_CONST_CAST(void *, &mqtt_client_info),
                                   &mqtt_client_info);
    SEGGER_SYSVIEW_PrintfHost("Mqtt Reconnect result %d\n", er);
  }
}

void mqtt_sub_request_cb(void *arg, err_t result)
{
  /* Just print the result code here for simplicity, 
     normal behaviour would be to take some action if subscribe fails like 
     notifying user, retry subscribe or disconnect from server */
  SEGGER_SYSVIEW_PrintfHost("Subscribe result: %d\n", result);
}

void ethernetif_notify_conn_changed(struct netif *netif)
{

  if (!(netif->ip_addr.addr))
  {
    SEGGER_SYSVIEW_PrintfHost("ethernetif_notify_conn_changed NULL");
  }
  else
  {
    PrintAddress(&netif->ip_addr);
    SEGGER_SYSVIEW_PrintfHost("\nethernetif_notify_conn_changed ip");
  }
}