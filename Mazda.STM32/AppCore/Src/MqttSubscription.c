
#include <string.h>
#include "mqtt.h"
#include "AppTasks.h"
#include "AppCore.h"

typedef void (*const CallbackFun)(const char *, u16_t);

struct mqtt_subscription_funs_s
{
    char *TopicName;
    CallbackFun Callback;
};

void mqtt_sub_request_cb(void *arg, err_t result);

#define TaskCount 7
static short FunctionIndex = -1;
static const struct mqtt_subscription_funs_s mqtt_subscription_funs_array[TaskCount] =
    {
        {"benchy", MqttBenchyIncomingFunction},
        {"key", KeyboardIncomingFunction},
        {"iGPIO", GPIOIncomingFunction},
        {"iAmp", VolumeControllerIncomingFunction},
        {"iLCD", LCDIncomingFunction},
        {"iBM64", BluetoothIncomingFunction},
        {"x86_pkg_temp", CPUTemperatureIncomingFunction}
};
//(*mqtt_subscription_funs_array[0].Callback)();

// arg oraz client  pochodzi z mqtt_connection_cb()
void SubscribeAll(mqtt_client_t *client, void *arg)
{
    err_t err;

    for (uint8_t i = 0; i < TaskCount; i++)
    {
        /* Subscribe to a topic named given from array with QoS level 1, call mqtt_sub_request_cb with result */

        err = mqtt_subscribe(client, mqtt_subscription_funs_array[i].TopicName, 1, mqtt_sub_request_cb, arg);
        if (err != ERR_OK)
        {
            printf("mqtt_subscribe return: %d\n", err);
        }
    }
}

int inpub_id;
void mqtt_incoming_data_cb(void *arg, const u8_t *data, u16_t len, u8_t flags)
{
    printf("Incoming publish payload with length %d, flags %u\n", len, (unsigned int)flags);

    if (flags & MQTT_DATA_FLAG_LAST)
    {
        if (0 <= FunctionIndex && FunctionIndex <= TaskCount)
        {
            (*mqtt_subscription_funs_array[FunctionIndex].Callback)((const char *)data, len);
            FunctionIndex = -1;
        }
    }
}

void mqtt_incoming_publish_cb(void *arg, const char *topic, u32_t tot_len)
{
    printf("Incoming publish at topic %s with total length %u\n", topic, (unsigned int)tot_len);

    for (uint8_t i = 0; i < TaskCount; i++)
    {
        if (strcmp(mqtt_subscription_funs_array[i].TopicName, topic) == 0)
        {
            FunctionIndex = i;
        }
    }
}