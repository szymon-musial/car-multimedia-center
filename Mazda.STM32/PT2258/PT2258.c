#include "PT2258.h"

uint8_t channell_address01[6] =
    {CHANNEL1_VOLUME_STEP_01, CHANNEL2_VOLUME_STEP_01, CHANNEL3_VOLUME_STEP_01,
     CHANNEL4_VOLUME_STEP_01, CHANNEL5_VOLUME_STEP_01, CHANNEL6_VOLUME_STEP_01};

uint8_t channell_address10[6] =
    {CHANNEL1_VOLUME_STEP_10, CHANNEL2_VOLUME_STEP_10, CHANNEL3_VOLUME_STEP_10,
     CHANNEL4_VOLUME_STEP_10, CHANNEL5_VOLUME_STEP_10, CHANNEL6_VOLUME_STEP_10};

uint16_t DevAddres;

// helper method
uint8_t HEX2BCD(uint8_t x)
{
  uint8_t y;
  y = (x / 10) << 4;
  y = y | (x % 10);
  return y;
}

void PT2258_init(uint16_t devAddres)
{
  DevAddres = (devAddres << 1);

  //  After Power is turned ON, PT2258 needs to wait for a short time in order to insure stability. The waiting
  //  time period for PT2258 to send I2
  //  C Bus Signal is at least 200ms. If the waiting time period is less than
  //  200ms, I2C Control may fail
  pt2258_delay(300);

  uint8_t arg[] = {
      // In order to ensure exact operation under any operating voltage, it is recommended an instruction to
      // clear the register “C0H” must be transmitted first
      CLEAR_REGISTER,
      ALL_CHANNELS_VOLUME_1STEP | (HEX2BCD(INIT_VOLUME) & 0x0f),
      ALL_CHANNELS_VOLUME_10STEP | ((HEX2BCD(INIT_VOLUME) & 0xf0) >> 4)};
  pt2258_send(DevAddres, arg, 3);
}

// Set mute: 1 -> mute on, 0 -> mute off
void PT2258_setMute(bool Mute)
{
  uint8_t arg[] = {MUTE | Mute};
  pt2258_send(DevAddres, arg, 1);
}

// Set channel volume, attenuation range : 0 to 79dB
void PT2258_setChannelVolume(uint8_t vol, uint8_t chno)
{
  // Parir for L and R channel
  chno *=2;
  uint8_t arg[] = {
    // R channel
    channell_address01[chno] | (HEX2BCD(vol) & 0x0f),
    channell_address10[chno] | ((HEX2BCD(vol) & 0xf0) >> 4),

    // L channel
    channell_address01[++chno] | (HEX2BCD(vol) & 0x0f),
    channell_address10[chno] | ((HEX2BCD(vol) & 0xf0) >> 4)
  };
  pt2258_send(DevAddres, arg, 4);
}

void PT2258_setAllVolume(uint8_t vol)
{
  uint8_t arg[] = {
      ALL_CHANNELS_VOLUME_1STEP | (HEX2BCD(vol) & 0x0f),
      ALL_CHANNELS_VOLUME_10STEP | ((HEX2BCD(vol) & 0xf0) >> 4)};
  pt2258_send(DevAddres, arg, 2);
}
