#include "AppTasks.h"
#include "adc.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "PT2258.h"
#include <stdlib.h>

#define QUEUE_LEN 5
#define PT2258_I2C_ADDRESS (uint16_t)64
#define NUMBER_OF_TRIES_BEFORE_APM_IS_READY 10
extern I2C_HandleTypeDef hi2c4;

App_AllocStaticQueue(VolumeControllerCmdQueue, 5, MQTTIncomingMessage_t)

uint8_t FindDevice();
void VolumeControllerTaskPreparation()
{
  App_PrepareStaticQueue(VolumeControllerCmdQueue);
}

void SetPT2258(const char *data, uint16_t len);
void VolumeControllerTaskFunction(void *arg)
{
  uint8_t FoundAddr = 0;

  for (uint8_t i = 0; i < NUMBER_OF_TRIES_BEFORE_APM_IS_READY; i++)
  {
      vTaskDelay(100 / portTICK_PERIOD_MS);
      FoundAddr = FindDevice();
      if(FoundAddr != 0)
      {
        break;
      }
  }
  PT2258_init(FoundAddr);

  MQTTIncomingMessage_t message;
  while (true)
  {
    if (xQueueReceive(VolumeControllerCmdQueue, &message, portMAX_DELAY) == pdTRUE)
    {
      SetPT2258(message.data, message.len);
    }
  }
}

void VolumeControllerIncomingFunction(const char *data, uint16_t len)
{
  MQTTIncomingMessage_t message;
  message.data = data;
  message.len = len;

  xQueueSend(VolumeControllerCmdQueue, &message, portMAX_DELAY);
}

uint8_t FindDevice()
{
  SEGGER_SYSVIEW_PrintfHost("Scanning I2C bus:");

  if (HAL_I2C_IsDeviceReady(&hi2c4, PT2258_I2C_ADDRESS << 1, 2, 2) != HAL_OK) // HAL_ERROR or HAL_BUSY or HAL_TIMEOUT
  {
    SEGGER_SYSVIEW_PrintfHost("Not found");
    return 0;
  }
  SEGGER_SYSVIEW_PrintfHost("Found DEC: %d", PT2258_I2C_ADDRESS); // Received an ACK at that address
  return PT2258_I2C_ADDRESS;
}

static char buff[2];

void SetPT2258(const char *data, uint16_t len)
{
  if (len == 1)
  {
    // mute
    int on = (int)*(data)-48;
    PT2258_setMute(on);
    buff[0] = *data;

    char topic[] = "oAmp/mute";
    AppSenderFunction(topic, buff, 1);
    return;
  }

  if (len == 3)
  {
    int channel = (int)*data - 48;
    char valueStr[] = {*(data + 1), *(data + 2)};
    int value = atoi(valueStr);

    if (!(0 <= channel && channel <= 3))
    {
      SEGGER_SYSVIEW_PrintfHost("SetPT2258.c\tBad channel number\n");
      return;
    }

    channel == 0 ? PT2258_setAllVolume(value) : PT2258_setChannelVolume(value, channel - 1);

    buff[0] = data[1];
    buff[1] = data[2];

    char topic[] = "oAmp/x";
    topic[5] = channel + '0';

    AppSenderFunction(topic, buff, 2);
    return;
  }
  SEGGER_SYSVIEW_PrintfHost("SetPT2258.c\tBad request");
}