#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "AppTasks.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define QUEUE_LEN 5
App_AllocStaticQueue(LcdCmdQueue, 5, MQTTIncomingMessage_t);

struct gpio_pin_mapping_s
{
  GPIO_TypeDef *Port;
  uint16_t PinNumber;
};
#define GPIO_PIN_MAPING_LENGTH 5
static struct gpio_pin_mapping_s GPIO_PIN_MAPING[GPIO_PIN_MAPING_LENGTH] = {
    {MCU_LCD_PIN1_GPIO_Port, MCU_LCD_PIN1_Pin},
    {MCU_LCD_PIN2_GPIO_Port, MCU_LCD_PIN2_Pin},
    {MCU_LCD_PIN3_GPIO_Port, MCU_LCD_PIN3_Pin},
    {MCU_LCD_PIN4_GPIO_Port, MCU_LCD_PIN4_Pin},
    {MCU_LCD_PIN5_GPIO_Port, MCU_LCD_PIN5_Pin},
};

void LCDTaskPreparation()
{
  App_PrepareStaticQueue(LcdCmdQueue);
}

void LCDParseMessage(const char *data, uint16_t len);

void LCDTaskFunction(void *arg)
{
  MQTTIncomingMessage_t message;
  while (true)
  {
    if (xQueueReceive(LcdCmdQueue, &message, portMAX_DELAY) == pdTRUE)
    {
      LCDParseMessage(message.data, message.len);
    }
  }
}

void LCDIncomingFunction(const char *data, uint16_t len)
{
  MQTTIncomingMessage_t message;
  message.data = data;
  message.len = len;

  xQueueSend(LcdCmdQueue, &message, portMAX_DELAY);
}



void SetLCD(int *pinNr)
{
  HAL_GPIO_WritePin(GPIO_PIN_MAPING[*pinNr].Port, GPIO_PIN_MAPING[*pinNr].PinNumber, GPIO_PIN_SET);
  vTaskDelay(100 / portTICK_PERIOD_MS);
  HAL_GPIO_WritePin(GPIO_PIN_MAPING[*pinNr].Port, GPIO_PIN_MAPING[*pinNr].PinNumber, GPIO_PIN_RESET);
  
  char topic[] = "oLCD/x";
  topic[5] = *pinNr + '0';

  char msg[] = "+";
  AppSenderFunction(topic, msg, strlen(msg));
}

void LCDParseMessage(const char *data, uint16_t len)
{
  if (len != 1)
  {
    printf("SetLCD.c\tBad request");
    return;
  }
  int pinNr = (int)*data - 48;

  if (!(0 <= pinNr && pinNr < GPIO_PIN_MAPING_LENGTH))
  {
    printf("SetLCD.c\tBad pin number");
    return;
  }  
  SetLCD(&pinNr);
}
