#include "AppTasks.h"
#include "adc.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define DMA_ADC_BUFFER_LENGTH 4096

uint16_t DMA_ADC_Buffer[DMA_ADC_BUFFER_LENGTH];

char resp[8];
uint16_t readCounter = 0;
unsigned long ADCMean = 0;

App_AllocStaticSemaphore(DMA_ADC_CollectionCompletedSemaphore)
void EncodeSWCKey(unsigned long ADCMean);

void SWCTaskPreparation()
{
    App_PrepareStaticSemaphore(DMA_ADC_CollectionCompletedSemaphore);
    HAL_ADC_Start_DMA(&hadc1, (uint32_t *)DMA_ADC_Buffer, DMA_ADC_BUFFER_LENGTH);
}

/// @brief FreeRTOS ADC Function
/// @param arg Task Arguments
void SWCTaskFunction(void *arg)
{
    while (true)
    {
        xSemaphoreTake(DMA_ADC_CollectionCompletedSemaphore, portMAX_DELAY);

        readCounter++;
        if (readCounter > 50)
        {
            // printf("Val %lu\n", res);
            readCounter = 0;
            int resp_len = sprintf(resp, "%lu", ADCMean);
            AppSenderFunction("oSWC", resp, resp_len);

            //SEGGER_SYSVIEW_Print(resp);
            EncodeSWCKey(ADCMean);
        }
    }
}

/// @brief ADC transfer over DMA completed callback
/// @param hadc ADC Device
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    // SEGGER_SYSVIEW_RecordEnterISR();
    long ADCMeanIncycle = 0;
    for (uint16_t i = 0; i < DMA_ADC_BUFFER_LENGTH; i++)
    {
        ADCMeanIncycle += DMA_ADC_Buffer[i];
    }
    ADCMeanIncycle /= DMA_ADC_BUFFER_LENGTH;
    readCounter++;

    if (readCounter > 50)
    {
        ADCMean = ADCMeanIncycle;
        xSemaphoreGiveFromISR(DMA_ADC_CollectionCompletedSemaphore, &xHigherPriorityTaskWoken);
    }
    // SEGGER_SYSVIEW_RecordExitISR();
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static uint8_t lastVolume = 79;
void EncodeSWCKey(unsigned long ADCMean)
{
    char msg[8];

    if(lastVolume > 79)
    {
        lastVolume = 79;
    }
    if(lastVolume < 79)
    {
        lastVolume = 0;
    }

    if(39000 < ADCMean && ADCMean < 40080)
    {
        // Up ?
        lastVolume = lastVolume - 10;    
        int resp_len = sprintf(msg, "0%02d", lastVolume);

        char* newArr = (char*) malloc(resp_len * sizeof(char));
        strncpy(newArr, msg, resp_len);

        VolumeControllerIncomingFunction(newArr, resp_len);
    }

    if(300 < ADCMean && ADCMean < 700)
    {
        // Down ?
        lastVolume += 10;    
        int resp_len = sprintf(msg, "0%02d", lastVolume);   

        char* newArr = (char*) malloc(resp_len * sizeof(char));
        strncpy(newArr, msg, resp_len);

        VolumeControllerIncomingFunction(newArr, resp_len);
    }
}