#include "stm32f7xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "AppTasks.h"
#include <stdlib.h>
#include <stdbool.h>

App_AllocStaticQueue(MqttBenchyCmdQueue, 5, MQTTIncomingMessage_t)

void MqttStres(int Sends)
{
    static char msg[20];
    for (int i = 0; i < Sends; i++)
    {
        int len = sprintf(msg, "%d", i);
        AppSenderFunction("benchy_res", msg, len);
    }
}

void MqttBenchyTaskPreparation()
{
    App_PrepareStaticQueue(MqttBenchyCmdQueue);
}

void MqttBenchyTaskFunction(void *arg)
{
    MQTTIncomingMessage_t message;
    while (true)
    {
        if (xQueueReceive(MqttBenchyCmdQueue, &message, portMAX_DELAY) == pdTRUE)
        {
            int size = atoi(message.data);
            MqttStres(size);            
        }
    }
}

void MqttBenchyIncomingFunction(const char *data, uint16_t len)
{
    MQTTIncomingMessage_t message;
    message.data = data;
    message.len = len;

    xQueueSend(MqttBenchyCmdQueue, &message, portMAX_DELAY);
}