#include "AppTasks.h"
#include "adc.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define QUEUE_LEN 5
static QueueHandle_t CPUTempCmdQueue = NULL;
App_AllocStaticQueue(CPUTempCmdQueue, 5, MQTTIncomingMessage_t)

#define PUMP_ACTIVATION_TEMP 60
#define WAIT_TIME_TO_TEMP_REPORT 5000 / portTICK_PERIOD_MS

void CPUTemperatureTaskPreparation()
{
    App_PrepareStaticQueue(CPUTempCmdQueue);
}

void CPUTemperatureTaskFunction(void *arg)
{
    MQTTIncomingMessage_t message;

    // On by default
    char msg[2] = "01";

    while (true)
    {
        // To prevent oscilation
        vTaskDelay(1000 / portTICK_PERIOD_MS);

        if (xQueueReceive(CPUTempCmdQueue, &message, WAIT_TIME_TO_TEMP_REPORT) == pdTRUE)
        {
            int currentCPUTemp = atoi(message.data);
            msg[1] = (currentCPUTemp > PUMP_ACTIVATION_TEMP) + '0';
        }
        else
        {
            msg[1] = '1';
        }
        GPIOIncomingFunction(msg, 2);
    }
}

void CPUTemperatureIncomingFunction(const char *data, uint16_t len)
{
    MQTTIncomingMessage_t message;
    message.data = data;
    message.len = len;

    xQueueSend(CPUTempCmdQueue, &message, portMAX_DELAY);
}
