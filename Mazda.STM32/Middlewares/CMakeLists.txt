cmake_minimum_required(VERSION 3.22)

#
# List of source files to compile
#
set(MiddlewaresSrc
    # Put here your source files, one in each line, relative to CMakeLists.txt file location
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/croutine.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/event_groups.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/list.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/queue.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/stream_buffer.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/tasks.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/timers.c

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1/port.c 

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2/cmsis_os2.c 
    
    ${CMAKE_CURRENT_LIST_DIR}/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c 
    ${CMAKE_CURRENT_LIST_DIR}/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c 
    ${CMAKE_CURRENT_LIST_DIR}/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c
    
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/SEGGER/SEGGER_RTT.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/SEGGER/SEGGER_RTT_printf.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/SEGGER/Syscalls/SEGGER_RTT_Syscalls_GCC.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/SEGGER/SEGGER_SYSVIEW.c 

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/SEGGER/SEGGER_RTT_ASM_ARMv7M.S

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/FreeRTOSV10/Config/Cortex-M/SEGGER_SYSVIEW_Config_FreeRTOS.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/FreeRTOSV10/SEGGER_SYSVIEW_FreeRTOS.c

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/auth.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/ccp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/chap_ms.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/chap-md5.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/chap-new.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/demand.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/eap.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/eui64.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/fsm.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/ipcp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/ipv6cp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/lcp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/magic.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/mppe.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/multilink.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/ppp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/pppapi.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/pppcrypt.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/pppoe.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/pppol2tp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/pppos.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/upap.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/utils.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/vj.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/bridgeif.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/bridgeif_fdb.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ethernet.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/lowpan6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/lowpan6_ble.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/lowpan6_common.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/slipif.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/zepif.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/netif/ppp/ecp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/api_lib.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/api_msg.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/err.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/if_api.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/netbuf.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/netdb.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/netifapi.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/sockets.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/api/tcpip.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/altcp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/altcp_alloc.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/altcp_tcp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/def.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/dns.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/inet_chksum.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/init.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ip.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/mem.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/memp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/netif.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/pbuf.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/raw.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/stats.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/sys.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/tcp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/tcp_in.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/tcp_out.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/timeouts.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/udp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/autoip.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/dhcp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/etharp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/icmp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/igmp.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/ip4.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/ip4_addr.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv4/ip4_frag.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/dhcp6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/ethip6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/icmp6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/inet6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/ip6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/ip6_addr.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/ip6_frag.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/mld6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/core/ipv6/nd6.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/system/OS/sys_arch.c 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/apps/mqtt/mqtt.c
)

#
# Include directories
#
set(MiddlewaresInc
    # Put here your include dirs, one in each line, relative to CMakeLists.txt file location

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/include 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1 

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 

    ${CMAKE_CURRENT_LIST_DIR}/ST/STM32_USB_Device_Library/Core/Inc 

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/SEGGER 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/FreeRTOSV10 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/SystemView/Config 

    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/system 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/netif/ppp 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/lwip 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/lwip/apps 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/lwip/priv 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/lwip/prot 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/netif 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/compat/posix 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/compat/posix/arpa 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/compat/posix/net 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/compat/posix/sys 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/src/include/compat/stdc 
    ${CMAKE_CURRENT_LIST_DIR}/Third_Party/LwIP/system/arch
)