﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Mazda.Shared.Domain;
using System;

namespace Mazda.Shared.CustomControls
{
    internal class MyBtTextView : CustomControlBase
    {
        #region ctor
        protected MyBtTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MyBtTextView(Context context) : base(context)
        {
        }

        public MyBtTextView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MyBtTextView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MyBtTextView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }
        #endregion
        TextView title, textView;
        Button button;
        private readonly string MqttSendMessage = "";
        private readonly string MqttSend = "";
        int MqttRecvBtEventType = default;

        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            string ButtonTitle = "";
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mybttextviewlayout, this, true);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.MyBtTextViewStyleable);
                MqttRecvBtEventType = ta.GetInteger(Resource.Styleable.MyBtTextViewStyleable_MqttRecvBtEventType, 0);
                textView = view.FindViewById<TextView>(Resource.Id.textview);
                ta.Recycle();
            }

            title = view.FindViewById<TextView>(Resource.Id.title);
            title.Text = CardTitle;

            button = view.FindViewById<Button>(Resource.Id.button);
            button.Text = ButtonTitle;
            button.Click += Button_Click;

        }

        private void Button_Click(object sender, EventArgs e)
        {
            mqttClient.Send(MqttSend, MqttSendMessage);
        }
        public override void Callback(string payload)
        {
            try
            {
                var EventId = MqttChannel.Split('/')[1];
                var EventIdInt = int.Parse(EventId, System.Globalization.NumberStyles.HexNumber);

                var btEvent = new BtEventType(EventIdInt, payload);
                if (btEvent.EventTypeId != MqttRecvBtEventType)
                {
                    return;
                }
                activity.RunOnUiThread(() => textView.Text = btEvent.ParamsName);
            }
            catch (Exception)
            {
            }
        }
    }
}