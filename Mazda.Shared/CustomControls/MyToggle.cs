﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Google.Android.Material.SwitchMaterial;
using System;

namespace Mazda.Shared.CustomControls
{
    internal class MyToggle : CustomControlBase
    {
        #region ctor
        protected MyToggle(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MyToggle(Context context) : base(context)
        {
        }

        public MyToggle(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MyToggle(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MyToggle(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }
        #endregion
        TextView title;
        SwitchMaterial switchMaterial;
        string MqttSendOnMessage = "", MqttSendOffMessage = "", MqttSend = "";
        string MqttRecvOnMessage = "0", MqttRecvOffMessage = "2";

        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mytogglereslayout, this, true);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.MyButtonToggleStyleable);
                MqttSend = ta.GetString(Resource.Styleable.MyButtonToggleStyleable_MqttSend);
                MqttSendOnMessage = ta.GetString(Resource.Styleable.MyButtonToggleStyleable_MqttSendOnMessage);
                MqttSendOffMessage = ta.GetString(Resource.Styleable.MyButtonToggleStyleable_MqttSendOffMessage);
                MqttRecvOnMessage = ta.GetString(Resource.Styleable.MyButtonToggleStyleable_MqttRecvOnMessage);
                MqttRecvOffMessage = ta.GetString(Resource.Styleable.MyButtonToggleStyleable_MqttRecvOffMessage);
                ta.Recycle();
            }

            title = view.FindViewById<TextView>(Resource.Id.title);
            title.Text = CardTitle;

            switchMaterial = view.FindViewById<SwitchMaterial>(Resource.Id.switchMaterial);
            switchMaterial.Text = "undef.";
            switchMaterial.CheckedChange += SwitchMaterial_CheckedChange;
        }

        private void SwitchMaterial_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
            => mqttClient.Send(MqttSend, e.IsChecked ? MqttSendOnMessage : MqttSendOffMessage);
        
        public override void Callback(string payload)
        {
            if(payload == MqttRecvOnMessage)
            {
                activity.RunOnUiThread(() => switchMaterial.Text = "On");
            }

            if (payload == MqttRecvOffMessage)
            {
                activity.RunOnUiThread(() => switchMaterial.Text = "Off");
            }
        }
    }
}