﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using System.Linq;
using System.Text;

namespace Mazda.Shared.CustomControls
{
    internal class MyButton : CustomControlBase
    {
        #region ctor
        protected MyButton(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MyButton(Context context) : base(context)
        {
        }

        public MyButton(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MyButton(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MyButton(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }
        #endregion
        TextView title;
        Button button;
        string MqttSendMessage = "", MqttSend = "";

        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            string ButtonTitle = "";
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mybuttonlayout, this, true);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.MyButtonStyleable);
                ButtonTitle = ta.GetString(Resource.Styleable.MyButtonStyleable_ButtonTitle) ?? "";
                MqttSend = ta.GetString(Resource.Styleable.MyButtonStyleable_MqttSend);
                MqttSendMessage = ta.GetString(Resource.Styleable.MyButtonStyleable_MqttSendMessage);
                if(string.IsNullOrEmpty(MqttSendMessage))
                {
                    var msgArrayString = ta.GetString(Resource.Styleable.MyButtonStyleable_MqttSendMessageArray);
                    var hexArray = msgArrayString.Split(',').Select(hex => byte.Parse(hex, System.Globalization.NumberStyles.HexNumber));

                    var sb = new StringBuilder();
                    foreach (var item in hexArray)
                    {
                        sb.Append( Convert.ToChar(item));
                    }

                    MqttSendMessage = sb.ToString();
                }
                ta.Recycle();
            }

            title = view.FindViewById<TextView>(Resource.Id.title);
            title.Text = CardTitle;

            button = view.FindViewById<Button>(Resource.Id.button);
            button.Text = ButtonTitle;
            button.Click += Button_Click;

        }

        private void Button_Click(object sender, EventArgs e)
        {
            mqttClient.Send(MqttSend, MqttSendMessage);
        }

        public override void Callback(string payload) { }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}