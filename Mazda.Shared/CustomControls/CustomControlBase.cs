﻿using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mazda.Shared.CustomControls
{
    public class CustomControlBase : FrameLayout
    {
        #region ctors
        protected CustomControlBase(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public CustomControlBase(Context context) : base(context) => Initialize(context);    
        public CustomControlBase(Context context, IAttributeSet attrs) : base(context, attrs) => Initialize(context, attrs);      
        public CustomControlBase(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) => Initialize(context, attrs);
        public CustomControlBase(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes) => Initialize(context, attrs);
        #endregion
        protected LayoutInflater layoutInflater;
        protected Activity activity;
        protected Context context;
        protected string CardTitle;
        protected MqttClient mqttClient;
        protected string MqttChannel;
        protected KeyValuePair<string, Action<string>> MySubscription;
        
        public virtual void Initialize(Context context, IAttributeSet attrs = null)
        {
            layoutInflater = (LayoutInflater)Context.GetSystemService(Context.LayoutInflaterService);
            this.context = context;
            activity = context as Activity;
            mqttClient = MqttClient.Instance;

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.CustomControlBaseStyleable);
                MqttChannel = ta.GetString(Resource.Styleable.CustomControlBaseStyleable_MqttRecv);
                CardTitle = ta.GetString(Resource.Styleable.CustomControlBaseStyleable_CardTitle);
                ta.Recycle();
                if (!string.IsNullOrEmpty(MqttChannel))
                {
                    Task.Run( async () => MySubscription = await mqttClient.SubscribeAsync(MqttChannel, Callback));
                }
            }
        }
        public virtual void Callback(string payload)
        {
            Console.WriteLine($"CustomControlBase default cb. Payload: {payload}");
        }

        protected override void OnDetachedFromWindow()
        {
            mqttClient.RemoveFromSubscriptions(MySubscription);
            base.OnDetachedFromWindow();
        }

    }
}