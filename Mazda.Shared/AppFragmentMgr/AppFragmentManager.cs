﻿using Android.Views;
using System.Linq;

namespace Mazda.Shared.AppFragmentMgr
{
    public class AppFragmentManager
    {
        private readonly MenuConfig[] _menuItemIdWithFragmentLayout;
        private readonly AndroidX.Fragment.App.FragmentManager _androidFragmentManager;
        private readonly int _containerViewId;

        public AppFragmentManager(MenuConfig[] menuItemIdWithFragmentLayout, AndroidX.Fragment.App.FragmentManager fragmentManager, int containerViewId)
        {
            _menuItemIdWithFragmentLayout = menuItemIdWithFragmentLayout;
            _androidFragmentManager = fragmentManager;
            _containerViewId = containerViewId;
        }

        public bool UpdateFragmentInFragmentManager(IMenuItem newItem)
        {
            int id = newItem.ItemId;
            MenuConfig nextMenuConfig = _menuItemIdWithFragmentLayout.FirstOrDefault(i => i.CallerId == id);

            if (nextMenuConfig != null)
            {
                var menuTransaction = _androidFragmentManager.BeginTransaction();
                menuTransaction.Replace(_containerViewId, nextMenuConfig.ViewFragment, nextMenuConfig.Tag);
                menuTransaction.Commit();
                return true;
            }

            return false;
        }
    }
}